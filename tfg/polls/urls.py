from django.conf.urls import url
from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView #esta es la vista del login
from django.contrib.auth import logout as do_logout
app_name = 'polls'

#Las siguientes direcciones se encuentran dentro de la app_name "polls"
urlpatterns = [
	#Se encuentran todas las consultas creadas
	url(r'^$', views.index, name="index"), #127.0.0.1:8000/polls/ 
	#Consulta con el id correspondiente
	url(r'^(?P<question_id>[0-9]+)/$', views.detail, name="detail"), #127.0.0.1:8000/polls/1
	#Votacion realiada
	url(r'^(?P<question_id>[0-9]+)/results$', views.results, name="results"), #127.0.0.1:8000/polls/1/results
	path('post/<int:question_id>', views.vote, name='vote'),
	#Cracion de la consulta
	path(r'createpoll/',views.createpoll,name="createpoll_url"),
	#Consulta guardada
	path(r'savedpoll/', views.savedpoll, name="savedpoll_url"), 
	#Vencedor de la consulta
	#Si no tiene participantes se elimina directamente la consulta
	url(r'^(?P<question_id>[0-9]+)/winner$', views.winner, name="winner"), 
	#Cierre de la consulta
	url(r'^(?P<question_id>[0-9]+)/closepoll$', views.closepoll, name="closepoll"), #127.0.0.1:8000/polls/1/closepoll
	#Página con el QR y el enlace a la cosulta
	url(r'(?P<question_id>[0-9]+)/qrlink$$', views.qrlink, name="qrlink"), 
	#Login
	path(r'login/',views.login,name="login_url"),
	#Registro
	path('register/',views.registerView,name="register_url"),
	#Logout
	path(r'logout/',views.logout,name="logout_url"),
	#Borrar consulta si no tiene participantes
	path('deletepoll/',views.deletepoll,name="deletepoll"),
	#Página principal
	path('',views.indexView,name='home'),
	#Página principal de interacción con el usuario registrado
	path('dashboard/',views.dashboardView,name="dashboard"),
]




