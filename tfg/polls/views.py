from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .models import Question, Choice
from django.urls import reverse 
from django.db.models import F, Value
from django.db.models.functions import Concat
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib.auth import logout as do_logout
from django.contrib import messages
from collections import defaultdict
import random

def indexView(request):
	return render(request, 'polls/index.html')

def login(request):

	form = AuthenticationForm()
	if request.method == "POST":
		form = AuthenticationForm(data=request.POST)

	if form.is_valid():	#Se comprueba el usuario y la contraseña

		username = form.cleaned_data['username']
		password = form.cleaned_data['password']


		user = authenticate(username=username, password=password)

		if user is not None:	#Si el usuario existe, se procede al inicio de sesión

			do_login(request, user)
			latest_questions = Question.objects.order_by('-pub_date')#[:5]	
			context =  {
			'latest_questions': latest_questions,
			}
			return render(request, 'polls/dashboard.html', context)


	return render(request, "polls/registration/login.html", {'form': form})

def logout(request):

	do_logout(request)
	form = AuthenticationForm
	return render(request, 'polls/dashboard.html',{'form':form})

def dashboardView(request):	#Vista principal de interacción del usuario registrado
	latest_questions = Question.objects.order_by('-pub_date')#[:5]	#Muestra las consultas que tiene activas y que haya creado el usuario
	question =  {
	'latest_questions': latest_questions,
	}


	return render(request, 'polls/dashboard.html',question)

def registerView(request):
	if request.method =="POST":
		form = UserCreationForm(request.POST)
		usernameNew = request.POST['username']

		if form.is_valid():	#Si el usuario se ha registrado correctamente se guarda su usuario y cintraseña 
			form.save()
			user = User.objects.get(username = usernameNew)
			user.is_staff = False
			user.is_admin = False
			user.is_superuser = True	#El usuario que se registra es superusuario para que pueda modificar la base de datos.
			user.save()
			autform = AuthenticationForm
			do_login(request, user)		#Una vez registrado también mantenemos que haya hecho el inicio de sesión
			return render(request, "polls/dashboard.html",{'form':autform})
	else:
		form = UserCreationForm()	

	return render(request, 'polls/registration/register.html',{'form':form}) #Si no se ha hecho correctamente se vuelve a la página de registro con los mensajes de error correspondientes

def index(request):	#En el index se encuentran todas las consultas creadas, pero no se utiliza esta vista para el proyecto
	latest_questions = Question.objects.order_by('-pub_date')#[:5]	
	context =  {
	'latest_questions': latest_questions,
	}
	return render(request, 'polls/index.html', context)

def qrlink(request, question_id):	#Pantalla de lectura del QR
	question = get_object_or_404(Question, pk = question_id)	
	return render(request, 'polls/qrlink.html', {'question': question})

def detail(request, question_id):
	question = get_object_or_404(Question, pk = question_id)	
	return render(request, 'polls/detail.html', {'question': question})

def closepoll(request,  question_id):	#Cierre de la consulta que se haya seleccionado
	question = get_object_or_404(Question, pk = question_id)	
	return render(request, 'polls/closepoll.html',{'question':question})

def results(request, question_id):	#Cuando se ha efectuado la votación, se muestra esta vista con un mensaje de que todo ha salido correctamente
	question = get_object_or_404(Question, pk=question_id)
	return render(request, 'polls/results.html', {'question': question})

def vote(request, question_id):		
	question = get_object_or_404(Question, pk=question_id)
	context = {
		'question': question,
	}


	maxItems = int(request.POST.get("numberOfElements", ""))
	idChoice = []
	for i in range(0, maxItems): #Con este for conseguimos todos los ID
		elem = str("choiceSelected") + str(i+1)
		idChoice.append(request.POST.get(elem,""))

	
	valueArray = [] #almacena en un array las values de todas las respuestas
	for j in range(0, maxItems):
		auxElem =  str("choice") + str(idChoice[j])
		valueArray.append(str(request.POST.get(auxElem,"")))
		
	for x in range(0, maxItems):#comprobar elementos repetidos
		for w in range(x+1, maxItems):
			if valueArray[x] == valueArray[w]:
				return render(request, 'polls/detail.html', {'question': question, 'error_message': "No se pueden repetir las opciones seleccionadas"})

	for a in range(0, maxItems):#almacenar respuestas
		for b in range(0, maxItems):
			if a+1 == int(valueArray[b]):
				question.all_votations += (str(idChoice[b]))
				question.all_votations += ','

	question.participants += 1
	question.total_of_answers += int(maxItems)
	question.save()


	return render(request,'polls/results.html', {'question': question})

def createpoll(request):
	return render(request, 'polls/createpoll.html', )

def deletepoll(request):	#Eliminación de la consulta si no ha tenido ningún participante
	return render(request, 'polls/deletepoll.html', )

def savedpoll(request):		#Creación de la consulta
	#Se tienen en cuenta una serie de controles de errores para que se pueda guardar correctamente la votación

	if not request.user.is_authenticated:
		messages.error(request, 'Tiene que estar registrado para crear una consulta')
		return render(request, 'polls/createpoll.html',)

	
	usernamePoll = str((request.POST.get("userName",""))) 	
	n = random.randint(0,1000)
	question_text = str((request.POST.get("questionName",""))) 
	if question_text == "":
		messages.error(request, 'Tiene que rellenar el nombre de la consulta')
		return render(request, 'polls/createpoll.html',)
	question = Question(n,question_text)
	question.creator = str(request.user.get_username())

	question.save()

	count = int((request.POST.get("count",""))) 
	for a in range(1, count):
		aux = str("textbox") + str(a)
		currentChoice = str((request.POST.get(aux,""))) 
		if currentChoice == "":
			messages.error(request, 'Tiene que rellenar los campos de las opciones')
			question.delete()
			return render(request, 'polls/createpoll.html',)
		choice = Choice(n+int(a),currentChoice,n)
		choice.save()

	
	
	return render(request, 'polls/savedpoll.html', {'question': question})


def winner(request, question_id):
	question = get_object_or_404(Question, pk=question_id)
	if question.participants != 0:	
		num_participants = int(question.participants)   		#numero de participantes en la encuesta
	else:
		question.delete()
		return render(request, 'polls/deletepoll.html', )
	aux_list = (question.all_votations).split(',')
	num_votes = int(question.total_of_answers)				#numero de votos emitidos
	

	results_elements_in_matrix = int(num_votes / num_participants)		#numero de choices de la pregunta
	
	if results_elements_in_matrix == 1:
		question.delete()
		return render(request, 'polls/deletepoll.html', )

	lista_nueva = []
	for i in range(0, len(aux_list), results_elements_in_matrix):
	    lista_nueva.append(aux_list[i:i+results_elements_in_matrix])  	#almacena en una lista todas las respuestas ordenadas
	lista_nueva.pop() 							#elimina el ultimo elemento el cual es una lista vacía


	matrix_of_distances = []						#calculo de la matriz de distancias
	elem_evaluated = str( int(question_id )+ 1)
	distance = 0


	for a in range(0,results_elements_in_matrix):
		for i in range(0,num_participants):
			for j in range(0,results_elements_in_matrix):	
				if elem_evaluated != lista_nueva[i][j]:
					
					matrix_of_distances.append(elem_evaluated)
					matrix_of_distances.append(lista_nueva[i][j])
					for o in range(0,results_elements_in_matrix):	
						if elem_evaluated == lista_nueva[i][o]:
							pos_elem_evaluated = o
					aux= j - pos_elem_evaluated		#calculamos la distancia entre el elemento evaluado y el otro elemento con el que se compara
					if aux < 0:		
						aux = 0;			
					matrix_of_distances.append(str(aux))
		elem_evaluated = str( int(elem_evaluated )+ 1)

	matrix_of_distances_aux = []
	for i in range(0, len(matrix_of_distances), 3):				#se separa de 3 en 3 ya que tenemos [id1, id2, distancia]
	    matrix_of_distances_aux.append(matrix_of_distances[i:i+3])  	
	matrix_of_distances_aux.pop() 

	matrix_of_distances = matrix_of_distances_aux        			#con esto conseguimos la matriz al separar los resultados de la lista en una matriz.
	
	for i in range(0,len(matrix_of_distances)):
		for j in range(0,len(matrix_of_distances)):	
			if i != j:
				if matrix_of_distances[i][0] == matrix_of_distances[j][0]:
					if matrix_of_distances[i][1] == matrix_of_distances[j][1]:
						matrix_of_distances[i][2] = str(int(matrix_of_distances[i][2]) + int(matrix_of_distances[j][2]))     #suma el elemento numero 3 de la lista que es la distancia
						matrix_of_distances[j][0]= '.'		#pone el primer elemento a "." para no volver a hacer la comparación y eliminarlo posteriormente
						

	
	for i in range(len(matrix_of_distances)-1,0,-1):
		if matrix_of_distances[i][0] == '.':
			matrix_of_distances.pop(i)				#elimina todos los elementos que tengan como primer elemento un "."
	
	grafo = matrix_of_distances						#generacion del grafo

	for i in range(0,len(grafo)):
		for j in range(0,len(grafo)):
			if i != j:
				if grafo[i][0] == grafo[j][1] and grafo[i][1] == grafo[j][0]:
					grafo[i][2] = str(int(grafo[i][2]) - int(grafo[j][2]))
					grafo[j][0] = '.'
	
	for i in range(len(grafo)-1,0,-1):					#eliminar elementos sobrantes
		if grafo[i][0] == '.':	
			grafo.pop(i)


	for i in range(0,len(grafo)):
		if (int(grafo[i][2]) < 0):
			aux1 = grafo[i][0] 
			aux2 = grafo[i][1] 
			grafo[i][0] = aux2
			grafo[i][1] = aux1
			grafo[i][2] = str(int(grafo[i][2]) * (-1))		#sustituye los elementos negativos por positivos y cambia el orden del peimer y segundo elemento


	dict_costes = {}
	for sublista in grafo:
		dict_costes[(sublista[0] , sublista[1])] = sublista[2]
	


		
	aux_list = []
	for i in range(0,results_elements_in_matrix):
		aux_list.append(str(int(question.id) + i + 1))

	
	candidate_wins = {}
	for candidato1 in aux_list:					#recorre todos los nombres de los candidatos
		victorias = 0
		
		for candidato2 in aux_list:				#vuelve a recorrer todos los nombres de los candidatos
			if candidato1 == candidato2:			#si los candidatos son iguales pasa al siguiente
				continue					#pasa a la siguiente iteracion del for
			candidate1_score = dict_costes.get((candidato1, candidato2), 0)		#el diccionario de costes, recupera el coste que tenemos del candidato 1 al candidato 2. Si no existiese devuvelve 0.
			candidato2 = dict_costes.get((candidato2, candidato1), 0)		#igual que el anterior pero del candidato 1 al 2
			if int(candidate1_score) > int(candidato2):
				victorias += 1

		if victorias not in candidate_wins.keys():
			candidate_wins[victorias] = []
		candidate_wins[victorias].append(candidato1)		#calcula el numero de victorias de un candidato, y añade el numero de victorias al candidato

	sorted_wins = sorted(candidate_wins.items(), reverse=True)	#metodo de python que ordena una lista
	sorted_wins = sorted_wins[0][1]

	vencedorSchulze = get_object_or_404(Choice, pk=int(sorted_wins[0])) #Almacenamos el vencedor en una variable
	consulta = question.question_text 

	#Almacenamos el vencedor en un .txt para que quede constancia del vencedor de la consulta y quien creó la consulta.
	winnerstxt_contenido = str("[ " + str(question.pub_date)+ " ]La consulta (" + str(question.question_text) +") cuyo creador  es el usuario: "+ str(question.creator) +" tiene como vencedor: " + str(vencedorSchulze) + "\n")
	f = open('polls/winners','a')
	f.write(winnerstxt_contenido)
	f.close()

	question.delete()
	context = {
		'consulta': consulta,
		'lista_nueva': lista_nueva,
		'matrix_of_distances': matrix_of_distances,
		'elem_evaluated': elem_evaluated,
		'sorted_wins': sorted_wins,
		'vencedorSchulze': vencedorSchulze,
	}	



	return render(request, 'polls/winner.html', context)
























