from django.db import models
from django import forms
from datetime import datetime  
from django.contrib.postgres.forms import SimpleArrayField
import qrcode
from qrcode.image.pure import PymagingImage
from io import BytesIO
from django.core.files import File
from PIL import Image, ImageDraw

class Question(models.Model): #Modelo donde se encuentran todos los datos necesarios para la Consulta
	question_text = models.CharField(max_length = 100)
	pub_date = models.DateTimeField(default=datetime.now, blank = True)
	all_votations = models.CharField(max_length = 1000,  blank = True)
	participants = models.IntegerField(default = 0, blank = True)
	total_of_answers = models.IntegerField(default = 0, blank = True)
	creator = models.CharField(max_length = 100, blank = True)
	qr_code = models.ImageField(upload_to='polls/static/polls', blank=True)


	def __str__ (self):
		return self.question_text

	def save(self, *args, **kwargs):  #Creacion del código QR
		input_data = '/polls/' + str(self.id) 
		qrcode_img = qrcode.make(input_data)
		canvas = Image.new('RGB', (290,290), 'white')
		draw = ImageDraw.Draw(canvas)
		canvas.paste(qrcode_img)
		fname = f'{self.id}.png'
		buffer = BytesIO()
		canvas.save(buffer,'PNG')
		self.qr_code.save(fname, File(buffer), save=False)
		canvas.close()
		super().save(*args, **kwargs)

class Choice(models.Model): 	#Respuesta a la consulta
	choice_text = models.CharField(max_length = 200)	
	question = models.ForeignKey(Question, on_delete=models.CASCADE)	#Clave foránea de la consulta, perteneciente a la "Question" de la que proviene
										#Si se elimina la "Question" se eliminan todas las "Choice" que tenga

	def __str__ (self):
  		return self.choice_text


