from django.shortcuts import render

def index(request):

	template = "home.html"
	context = {}
	return render(request, 'home/index.html', context)
