from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from django.conf.urls import include 

#Todas las URLs a las que se pueden acceder
urlpatterns = [
    path('', include('home.urls', namespace='home')),
    path('admin/', admin.site.urls), 
    path('polls/', include('polls.urls', namespace="polls")),
]
