# Aplicación web para realizar consultas aplicando el método Schulze

Realizada por: Andrés Alcántara Alcántara

## Instalación

Instalar virtualenv:

    pip3 install virtualenv

En el directorio donde se encuentra el fichero tfg, al mismo nivel creamos un entorno virtual:

    virtualenv -p python3 env

Meterte en el env y acticarlo (siempre hay que hacerlo antes del runserver):

    source env/bin/activate

Instalar django:

    pip install django

## Iniciar el servidor

Desde la carpeta del tfg:

    python manage.py runserver
    # o cambiando el puerto (opcional)
    python manage.py runserver 8080

Crear nueva carpeta:

    python manage.py startapp users

Guardar cambios del models (polls):

    python manage.py makemigrations polls

Migrations:

    python manage.py migrate


Acceder a la base de datos desde el terminal:

    python manage.py shell

Crear superuser (para acceder al localhost:8000/admin):

    python manage.py createsuperuser

Instalar crispy forms:

    pip install django-crispy-forms
